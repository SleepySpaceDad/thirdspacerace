import random, math, pygame, sys
from enum import Enum

DIST_TO_PROXIMA = 1302
SECOND = 1000
TRAVEL_TIME = SECOND * 3
MINING_TIME = SECOND * 15
WIDTH = 600
HEIGHT = 480
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 170, 24)
RED = (255, 0, 0)

class Asteroid():
    def __init__(self):
        self.pos = [random.randint(WIDTH, WIDTH + 100), random.randint(0, HEIGHT - 60)]
        self.state = 0
        self.speed = random.randint(1, 3)

class ShipStatus():
    def __init__(self):
        self.duration = 0
        self.foodModifier = 1
        self.fuelModifier = 1
    
    def update(self):
        if(self.duration > 0):
            self.duration -= 1
            if(self.duration == 0):
                self.__init__()

class Ship():
    def __init__(self):
        self.fuel = 500
        self.food = 5000
        self.status = ShipStatus()
        self.people = 1000
        self.alive = True

    def update(self, distance):
        self.status.update()

        self.fuel -= math.ceil(distance * self.status.fuelModifier)
        if(self.fuel < 0):
            self.fuel = 0
        
        self.people -= ((self.people // 1000) + 1) * random.randint(5, 7)

        #if there's enough food the population will grow
        if self.food > self.people * 1.25:
            self.people += ((self.people // 1000) + 1) * random.randint(10, 30)

            #cap population at 99999
            if(self.people > 99999):
                self.people = 99999

        self.food += math.ceil(self.people * (1.1 / self.status.foodModifier))
        #cap food at 99999
        if(self.food > 99999):
            self.food = 99999

        self.food -= self.people
        if(self.food < 0):
            self.food = 0

class Button():
    def __init__(self, pos):
        self.pressed = False
        self.pos = pos

class GameState(Enum):
    WAITING = 0
    TRAVELING = 1
    MINING = 2
    GAME_OVER = 3
    COMPLETE = 4

class Game():
    def __init__(self):
        self.dist_remaining = DIST_TO_PROXIMA
        self.ship = Ship()
        self.running = True
        self.stars = []
        self.buttons = [Button((499, 469)), Button((561, 469))]
        self.state = GameState.WAITING
        self.reset_state_time = 0
        self.months = 0
        self.asteroids = None
        self.frame = 0

    def run(self):
        self.init()
        self.loop()
        self.clean_up()

    def init(self):
        pygame.init()
        pygame.display.set_caption("The Third Space Race")
        self.surface = pygame.display.set_mode((WIDTH, HEIGHT), 0 , 32)
        self.terminal_font = pygame.font.Font("assets/LCD_Solid.ttf", 10)
        self.frame_clock = pygame.time.Clock()
        
        self.load_images()
        self.gen_stars()

    def load_images(self):
        self.star_img = pygame.image.load("assets/star.png").convert_alpha()
        self.ship_img = pygame.image.load("assets/ship.png").convert_alpha()
        self.ui_img = pygame.image.load("assets/ui.png").convert_alpha()
        self.button_down_img = pygame.image.load("assets/button_down.png")
        self.button_up_img = pygame.image.load("assets/button_up.png")
        self.asteroid_img = pygame.image.load("assets/asteroid.png")
        self.asteroid_crack1_img = pygame.image.load("assets/asteroid_crack1.png")
        self.asteroid_crack2_img = pygame.image.load("assets/asteroid_crack2.png")
        self.asteroid_crack3_img = pygame.image.load("assets/asteroid_crack3.png")
        self.asteroid_crack4_img = pygame.image.load("assets/asteroid_crack4.png")
        self.asteroid_crack5_img = pygame.image.load("assets/asteroid_crack5.png")
        self.asteroid_crack6_img = pygame.image.load("assets/asteroid_crack6.png")
        self.asteroid_crack7_img = pygame.image.load("assets/asteroid_crack7.png")
        self.asteroid_crack8_img = pygame.image.load("assets/asteroid_crack8.png")
        self.fire1_img = pygame.image.load("assets/fire1.png")
        self.fire2_img = pygame.image.load("assets/fire2.png") 

    def gen_stars(self):
        i = 0
        while i < (WIDTH * HEIGHT) / 1000:
            self.stars.append([random.randint(0, WIDTH - 1), random.randint(0, HEIGHT - 1), random.randint(6, 9)])
            i += 1

    def loop(self):
        while self.running:
            self.render()
            self.process()


    def render(self):
        self.surface.fill(BLACK)

        self.render_stars()
        self.render_interface()
        self.render_ship()
        self.render_asteroids()

        #ensure framerate
        self.frame_clock.tick_busy_loop(30)
        pygame.display.update()
        self.frame += 1

    def render_image(self, image, pos):
        rect = image.get_rect()
        rect.left = pos[0]
        rect.top = pos[1]
        self.surface.blit(image, rect)

    def render_stars(self):
        #draw stars
        for star in self.stars:
            self.render_image(self.star_img, (star[0], star[1]))
    
    def render_asteroids(self):
        if(self.asteroids != None):
            for asteroid in self.asteroids:
                image = None
                if(asteroid.state == 0):
                    image = self.asteroid_img
                elif(asteroid.state == 1):
                    image = self.asteroid_crack1_img
                elif(asteroid.state == 2):
                    image = self.asteroid_crack2_img
                elif(asteroid.state == 3):
                    image = self.asteroid_crack3_img
                elif(asteroid.state == 4):
                    image = self.asteroid_crack4_img
                elif(asteroid.state == 5):
                    image = self.asteroid_crack5_img
                elif(asteroid.state == 6):
                    image = self.asteroid_crack6_img
                elif(asteroid.state == 7):
                    image = self.asteroid_crack7_img
                else:
                    image = self.asteroid_crack8_img
                
                self.render_image(image, asteroid.pos)

    def render_interface(self):
        self.render_image(self.ui_img, (0, 450))
        
        text_y = 467

        self.render_terminal_text("{} milliparsecs".format(self.dist_remaining), (9, text_y))
        self.render_terminal_text("{:0>5d}".format(self.ship.people), (158, text_y))
        self.render_terminal_text("{:0>5d}".format(self.ship.food), (207, text_y))
        self.render_terminal_text("{:0>5d}".format(self.ship.fuel), (256, text_y))
        self.render_terminal_text("{:0>5d}".format(self.months), (305, text_y))

        self.render_buttons()

        if(self.state == GameState.GAME_OVER):
            text = self.terminal_font.render("GAME OVER", True, RED, None)
            textRect = text.get_rect()
            textRect.centerx = self.surface.get_rect().centerx
            textRect.centery = self.surface.get_rect().centery + 40
            self.surface.blit(text, textRect)
        
        if(self.state == GameState.COMPLETE):
            text = self.terminal_font.render("SUCCESS!", True, GREEN, None)
            textRect = text.get_rect()
            textRect.centerx = self.surface.get_rect().centerx
            textRect.centery = self.surface.get_rect().centery + 40
            self.surface.blit(text, textRect)

    def render_terminal_text(self, text, pos):
        text = self.terminal_font.render(text, True, GREEN, None)
        textRect = text.get_rect()
        textRect.left = pos[0]
        textRect.top = pos[1]
        self.surface.blit(text, textRect)

    def render_buttons(self):
        for button in self.buttons:
            if(button.pressed):
                self.render_image(self.button_down_img, button.pos)
            else:
                self.render_image(self.button_up_img, button.pos)

    def render_ship(self):
        self.render_image(self.ship_img, (246, 224))
        
        if(self.state == GameState.TRAVELING):
            fire_frame = self.frame % 12
            fire = None
            if(fire_frame < 6):
                fire = self.fire1_img
            else:
                fire = self.fire2_img
            self.render_image(fire, (206, 224))

    def process(self):
        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                self.running = False

            if(event.type == pygame.MOUSEBUTTONDOWN):
                self.process_click_down(event)
            
            if(event.type == pygame.MOUSEBUTTONUP):
                self.process_click_up(event)

        if(self.state != GameState.GAME_OVER and self.state != GameState.COMPLETE):
            if(self.state == GameState.WAITING):
                if(self.buttons[0].pressed):
                    self.state = GameState.MINING
                    self.reset_state_time = pygame.time.get_ticks() + MINING_TIME
                elif(self.buttons[1].pressed):
                    self.state = GameState.TRAVELING
                    self.reset_state_time = pygame.time.get_ticks() + TRAVEL_TIME

            if(self.state == GameState.TRAVELING):
                self.update_stars()
            elif(self.state == GameState.MINING):
                self.update_asteroids()

            if((self.state == GameState.TRAVELING and pygame.time.get_ticks() >= self.reset_state_time) or \
            (self.state == GameState.MINING and self.asteroids == None)):
                self.turn()
                self.state = GameState.WAITING

            if(self.ship.fuel == 0 or self.ship.food == 0 or self.ship.people == 0):
                self.state = GameState.GAME_OVER

            if(self.dist_remaining == 0):
                self.state = GameState.COMPLETE

    def process_click_down(self, mouse_click):
        if(mouse_click.button == 1):
            if(self.state == GameState.WAITING):
                for button in self.buttons:
                    if(button.pos[0] <= mouse_click.pos[0] and mouse_click.pos[0] <= button.pos[0] + 19 \
                    and button.pos[1] <= mouse_click.pos[1] and mouse_click.pos[1] <= button.pos[1] + 19):
                        button.pressed = True
            if(self.state == GameState.MINING):
                for asteroid in self.asteroids:
                    if(asteroid.pos[0] <= mouse_click.pos[0] and mouse_click.pos[0] <= asteroid.pos[0] + 30 \
                    and asteroid.pos[1] <= mouse_click.pos[1] and mouse_click.pos[1] <= asteroid.pos[1] + 30):
                        asteroid.state += 1
    
    def process_click_up(self, mouse_click):
        if(mouse_click.button == 1):
            for button in self.buttons:
                button.pressed = False

    def update_stars(self):
        for star in self.stars:
            star[0] -= star[2]

            if(star[0] < 0):
                star[0] = WIDTH - 1
                star[1] = random.randint(0, HEIGHT - 1)
                star[2] = random.randint(6, 9)

    def update_asteroids(self):
        if(self.state == GameState.MINING and self.asteroids == None):
            self.gen_asteroids()

        passed_asteroids = 0

        if(self.asteroids != None):
            for asteroid in self.asteroids:
                asteroid.pos[0] -= asteroid.speed

                if((asteroid.pos[0] <= -30 or asteroid.state >= 9) and self.state == GameState.MINING):
                    if(asteroid.state >= 9):
                        self.ship.fuel += random.randint(5, 10)

                        if(self.ship.fuel >= 99999):
                            self.ship.fuel = 99999

                        self.ship.food += random.randint(100, 500)

                        if(self.ship.food >= 99999):
                            self.ship.food = 99999

                    if((asteroid.pos[0] <= -30 or asteroid.state >= 9) and pygame.time.get_ticks() >= self.reset_state_time):
                        passed_asteroids += 1

                        #put the asteroid in a null state
                        asteroid.state = 0
                        asteroid.pos = [-30, -30]
                    else:
                        asteroid.__init__()

            if(passed_asteroids == 20):
                self.asteroids = None

    def gen_asteroids(self):
        self.asteroids = []
        i = 0

        while i < 20:
            self.asteroids.append(Asteroid())
            i += 1

    def turn(self):
        distance = 0

        if(self.state == GameState.TRAVELING):
            distance = random.randint(40, 50)

        self.ship.update(distance)
        self.dist_remaining -= distance
        self.months += 1

        if(self.ship.status.duration == 0):
            self.random_event()

    def random_event(self):
        event = random.randint(0, 9)
        duration = 0

        if(event == 7):
            duration = random.randint(2, 8)
            self.ship.status.foodModifier = 4        
        elif(event == 8):
            duration = random.randint(2, 4)
            self.ship.status.fuelModifier = 0.5
        elif(event == 9):
            duration = random.randint(2, 4)
            self.ship.status.fuelModifier = 2
        
        self.ship.status.duration = duration

    def clean_up(self):
        pygame.quit()

game = Game()
game.run()
