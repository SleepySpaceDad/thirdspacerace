#The Third Space Race
This was a project for my GAME101 course at GMU. It's a simple game made over the course of several weeks. Doing this project taught me how to do game-loop programming, and it's my first completed game. The programming and artwork was done all by myself. 

The font used is LCD Solid, which I found here: https://www.fontspace.com/lcd-solid/lcd-solid